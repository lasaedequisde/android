package com.controlsangre.sangre.database;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.view.View;

import com.controlsangre.sangre.R;
import com.controlsangre.sangre.farmaciasJSON.lista_farmacias;
import com.controlsangre.sangre.preferencias;


public class MainActivity extends Activity implements View.OnCreateContextMenuListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view){

        Intent i = new Intent(this, Agregar.class);
        startActivity(i);
    }

    public void onListar(View view){
        Intent i = new Intent(this, BuscarPersona.class);
        startActivity(i);
    }

    public void onEliminar(View view){
        Intent i = new Intent(this, eliminar.class);
        startActivity(i);
    }

    public void premodificar(View view){
        Intent i = new Intent(this, pre_modificar.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = null;

        switch (item.getItemId()) {
            case R.id.menu_preferencias:
                intent = new Intent(MainActivity.this, preferencias.class);
                startActivity(intent);
                return true;
            case R.id.menu_maps:
                intent = new Intent(this, lista_farmacias.class);
                startActivity(intent);
                return true;
            case R.id.menu_acerca_de:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.acerca_de_message)
                        .setTitle(R.string.acerca_de_title)
                        .setPositiveButton(R.string.btAceptar, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialogo = builder.create();
                dialogo.show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
