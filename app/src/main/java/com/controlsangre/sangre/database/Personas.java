package com.controlsangre.sangre.database;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by A on 01/05/2015.
 */
public class Personas implements Parcelable{

    private int _id;
    private String _nombre;
    private String _apellido;
    private int _edad;
    private String _telefono;
    private String _email;
    private String  _tiposangre;

    private Bitmap imagen;
    private boolean cbSemanal;

    public Personas(){
        //Constructor Vacio
    }
    public Personas(String nombre, String apellido, int edad, String telefono, String email,
                    String tiposangre) {
        this._nombre = nombre;
        this._apellido = apellido;
        this._edad = edad;
        this._telefono = telefono;
        this._email = email;
        this._tiposangre = tiposangre;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_nombre() {
        return _nombre;
    }

    public void set_nombre(String _nombre) {
        this._nombre = _nombre;
    }

    public String get_apellido() {
        return _apellido;
    }

    public void set_apellido(String _apellido) {
        this._apellido = _apellido;
    }

    public int get_edad() {
        return _edad;
    }

    public void set_edad(int _edad) {
        this._edad = _edad;
    }

    public String get_telefono() {
        return _telefono;
    }

    public void set_telefono(String _telefono) {
        this._telefono = _telefono;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public String get_tiposangre() {
        return _tiposangre;
    }

    public void set_tiposangre(String _tiposangre) {
        this._tiposangre = _tiposangre;
    }

    public Bitmap getImagen() {
        return imagen;
    }

    public void setImagen(Bitmap imagen) {
        this.imagen = imagen;
    }

    public boolean isCbSemanal() {
        return cbSemanal;
    }

    public void setCbSemanal(boolean cbSemanal) {
        this.cbSemanal = cbSemanal;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destino, int flag) {
        destino.writeParcelable(imagen, PARCELABLE_WRITE_RETURN_VALUE);
        destino.writeString(String.valueOf(cbSemanal));
    }

    private Personas(Parcel entrada) {
        imagen = entrada.readParcelable(Bitmap.class.getClassLoader());
        cbSemanal = Boolean.parseBoolean(entrada.readString());
    }

    public static final Parcelable.Creator<Personas> CREATOR = new Parcelable.Creator<Personas>() {

        public Personas createFromParcel(Parcel in) {
            return new Personas(in);
        }

        public Personas[] newArray(int size) {
            return new Personas[size];
        }
    };


}
