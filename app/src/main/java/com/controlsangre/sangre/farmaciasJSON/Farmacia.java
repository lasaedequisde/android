package com.controlsangre.sangre.farmaciasJSON;

/**
 * Created by arN on 10/03/2016.
 */
public class Farmacia {

    private String nombre;
    private String descripcion;

    private float latitud;
    private float longitud;

    public Farmacia(){

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getLatitud() {
        return latitud;
    }

    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    public float getLongitud() {
        return longitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }
}
