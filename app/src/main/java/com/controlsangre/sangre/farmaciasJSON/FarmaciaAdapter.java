package com.controlsangre.sangre.farmaciasJSON;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.controlsangre.sangre.R;

import java.util.List;


public class FarmaciaAdapter  extends ArrayAdapter<Farmacia> {

    private Context contexto;
    private int layoutId;
    private List<Farmacia> datos;

    public FarmaciaAdapter(Context contexto, int layoutId, List<Farmacia> datos) {
        super(contexto, layoutId, datos);

        this.contexto = contexto;
        this.layoutId = layoutId;
        this.datos = datos;
    }

    @Override
    public View getView(int posicion, View view, ViewGroup padre) {

        View fila = view;
        ItemFarmacia item = null;

        if (fila == null) {
            LayoutInflater inflater = ((Activity) contexto).getLayoutInflater();
            fila = inflater.inflate(layoutId, padre, false);

            item = new ItemFarmacia();
            item.imagen = (ImageView) fila.findViewById(R.id.ivFarmacia);
            item.nombre = (TextView) fila.findViewById(R.id.lbNombreFarmacia);
            item.descripcion = (TextView) fila.findViewById(R.id.lbDescripcionFarmacia);

            fila.setTag(item);
        }
        else {
            item = (ItemFarmacia) fila.getTag();
        }

        Farmacia farmacia = datos.get(posicion);
        item.imagen.setImageDrawable(contexto.getResources().getDrawable(R.drawable.ico_farmacia));
        item.nombre.setText(farmacia.getNombre());
        item.descripcion.setText(farmacia.getDescripcion());

        return fila;
    }

    static class ItemFarmacia {

        ImageView imagen;
        TextView nombre;
        TextView descripcion;

    }



}
