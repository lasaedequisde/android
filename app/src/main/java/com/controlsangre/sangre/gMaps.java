package com.controlsangre.sangre;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.controlsangre.sangre.farmaciasJSON.Util;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class gMaps extends Activity{

    GoogleMap mMap;

    private double latitud;
    private double longitud;
    private String nombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gmaps);

        Intent i = getIntent();
        latitud = i.getFloatExtra("latitud", 0);
        longitud = i.getFloatExtra("longitud", 0);
        nombre = i.getStringExtra("nombre");

        // Transforma las coordenadas al sistema LatLng y las almacena
        uk.me.jstott.jcoord.LatLng ubicacion = Util.DeUMTSaLatLng(latitud, longitud, 'N', 30);
        this.latitud = ubicacion.getLat();
        this.longitud = ubicacion.getLng();

        // Inicializa el sistema de mapas de Google
        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Obtiene una referencia al objeto que permite "manejar" el mapa
        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
    }

    @Override
    public void onResume() {
        super.onResume();

        ubicarFarmacia();
    }

    /**
     * Marca el restaurante elegido en el mapa
     */
    private void ubicarFarmacia() {

        // Obtiene una vista de c�mara
        CameraUpdate camara =
                CameraUpdateFactory.newLatLng(new LatLng(latitud, longitud));

        // Coloca la vista del mapa sobre la posici�n del restaurante
        // y activa el zoom para verlo de cerca
        mMap.moveCamera(camara);
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));

        // A�ade una marca en la posici�n del restaurante con el nombre de �ste
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitud, longitud))
                .title(nombre));
    }


}

