package com.controlsangre.sangre;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.controlsangre.sangre.database.MainActivity;


public class main extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        findViewById(R.id.btBBDD).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(main.this, MainActivity.class);
                startActivity(intent);

            }
        });

    }
}
